<header class="pt-4 shadow">
  <nav class="py-2.5">
    <div class="flex flex-wrap items-center lg:justify-between justify-center max-w-screen-xl px-4 mx-auto">
      <a href="#" class="flex items-center">
        <img src="<?php echo $URI->base("/assets/img/logo.svg"); ?>" class="h-6 mr-3 sm:h-9" alt="Pintow Logo" />
      </a>
      <div class="flex flex-wrap justify-center items-center lg:order-2">
        <a href="#" class="text-white hover:bg-gray-50 focus:ring-4 focus:ring-gray-300 font-medium rounded-lg text-sm px-4 lg:px-5 py-2 lg:py-2.5 sm:mr-2 focus:outline-none">Participe do Pré-Lançamento faça o seu</a>
        <a href="#" class="text-white ring-1 ring-white font-medium rounded-3xl text-sm px-4 lg:px-5 py-2 lg:py-2.5 sm:mr-2 lg:mr-0 focus:outline-none">Cadastre Agora</a>
      </div>
    </div>
  </nav>
</header>